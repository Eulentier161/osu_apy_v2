class ApiError(Exception):
    pass


class NotFound(Exception):
    pass
