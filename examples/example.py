from pprint import pprint

import requests
import yaml
from osu_apy_v2 import OsuApiV2


def top_play(username: str, mode: str = "osu", limit: int = 1) -> requests.Response:
    with open("./config.yaml") as f:
        cfg = yaml.safe_load(f)
    api = OsuApiV2(cfg["id"], cfg["secret"])
    # return api.get(f"/users/{api.get_user_id(username)}/scores/best?mode={mode}&limit={limit}")
    return api.get_user_scores(username)


if __name__ == "__main__":
    pprint(top_play("whitecat").json())
